<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/xmlrpc?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_description' => 'Configuración del servidor xml-rpc',

	// E
	'erreur_arguments_obligatoires' => 'Error: los siguientes campos son obligatorios "@arguments@"',
	'erreur_identifiant' => 'Ha de proporcionar el identificador numérico del objeto (@objet@)',
	'erreur_impossible_lire_objet' => 'Error: es imposible leer el objeto "@objet@" #@id_objet@',
	'erreur_lecture' => 'Error de lectura del objeto (@objet@ #@id_objet@)',
	'erreur_mauvaise_identification' => 'Error en la identificación (usuario/contraseña)',
	'erreur_objet_inexistant' => 'El objeto solicitado no existe (@objet@ #@id_objet@)',
	'erreur_xmlrpc_desactive' => 'El servidor xml-rpc esta desactivado',

	// L
	'label_api_preferee' => 'API de edición preferida',
	'label_desactiver_rsd' => 'Desactivar la RSD',
	'label_desactiver_rsd_long' => 'Desactiva el uso del archivo RSD en el encabezado de las páginas',
	'label_ferme' => 'Desactivar el servidor'
);
