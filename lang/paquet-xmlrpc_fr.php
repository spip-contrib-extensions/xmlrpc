<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/xmlrpc.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// X
	'xmlrpc_description' => 'API xml-rpc cliente et serveur',
	'xmlrpc_slogan' => 'Serveur et client xml-rpc'
);
